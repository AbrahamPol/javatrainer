create database mantenimientoSandra;
use mantenimientoSandra;

create table equipo(
noEquipo int primary key,
marca text,
id_responsable int,
fechaRegistro text,
fechaUltimoMantenimiento text,
status boolean
)engine=innodb;


create table responsable(
id_responsable int primary key,
nombre text,
area text,
status boolean
)engine=innodb;

create table bitacoraMantenimiento(
id int primary key,
noEquipo int,
fecha text,
descripcion text,
observaciones text,
estado text,
status boolean
)engine=innodb;

ALTER TABLE bitacoraMantenimiento ADD FOREIGN KEY (noEquipo) REFERENCES equipo (noEquipo) on delete cascade on update cascade;
ALTER TABLE equipo ADD FOREIGN KEY (id_responsable) REFERENCES responsable (id_responsable) on delete cascade on update cascade;



insert into responsable values (100,'Omar','5AC',1);
insert into responsable values (200,'Roman','5VI',1);
insert into responsable values (300,'Gabriela','5AC',1);
insert into responsable values (400,'Norma','5VI',1);

insert into equipo values (1000,'Acen',100,'2015-10-5','2018-10-30',1);
insert into equipo values (2000,'Mi',100,'2015-12-6','2018-10-31',1);
insert into equipo values (3000,'Dell',200,'2016-4-21','2018-11-14',1);
insert into equipo values (4000,'Acen',300,'2017-8-14','2018-12-5',1);

insert into bitacoraMantenimiento values (1,1000,'2018-10-30', 'limpieza fisica','ninguna','terminado',1);
insert into bitacoraMantenimiento values (2,2000,'2018-10-31', 'formateo','requiere mas RAM','terminado',1);
insert into bitacoraMantenimiento values (3,3000,'2018-11-14', 'vacunacion','ninguna','terminado',1);
insert into bitacoraMantenimiento values (4,4000,'2018-12-5', 'desfragmentacion','disco duro lleno','terminado',1);
insert into bitacoraMantenimiento values (5,1000,'2018-03-8', 'instalacion de sw',' ','en proceso',1);
insert into bitacoraMantenimiento values (6,2000,'2018-03-8', 'formateo',' ','en proceso',1);


	select * from wishlist wl
	inner join listacompartida l
	on wl.id_wishlist=l.id_wishlist
	inner join detalle_wishlist dwl
	on dwl.id_wishlist=l.id_wishlist
	inner join libro li
	on li.id_libro=dwl.id_libro
	where wl.nombreW="sandias";

	insert into equipo values (400,'Acen',300,curdate(),'2018-12-5',1);





select eq.marca,re.nombre,re.area
from equipo eq
inner join responsable re
on eq.id_responsable=re.id_responsable;








