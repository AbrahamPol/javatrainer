
package BD;

/**
 *
 * @author Abraham
 */
abstract public class EquipoMetodos {
    
    /**
     * Metodos que se necsitan para trabajar
     */
    
    /**
     * 
     *Find All 
     */
    abstract public void findByAll();
    
    /**
     * 
     * @param id 
     */
    abstract public void findById(int id);
    /**
     * 
     * @param marca 
     */
    abstract public void findByMarca(String marca);
    /**
     * 
     * @param id 
     */
    abstract public void deleteById(int id);
   /**
    * 
    * @param modelo 
    */
    abstract public void updateteById(ModeloEquipo modelo);
    /**
     * 
     * @param modelo 
     */
    abstract public void save(ModeloEquipo modelo);
}
