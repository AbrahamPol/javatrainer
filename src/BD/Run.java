package BD;

/**
 *
 * @author Abraham
 */
public class Run {

    public static void main(String[] args) {
        /**
         * Main interpreta la vista, en este caso un boton o en este caso
         * representa la peticion del servlet ya sea POST O GET
         */
        EquipoMetodos equipo = new EquipoController();
        //Find
//      equipo.findById(1000);
//      equipo.findByMarca("k");
//      equipo.findByAll();

        //INSERT
        /**
         * Existen dos maneras de instanciar un modelo 1.-A traves de un
         * constructor por defecto 2.-A traves de sus metodos de acceso SET
         */
        //1.- CONSTRUCTOR
        // ModeloEquipo modelo= new ModeloEquipo(6000,"HP",100,"curdate()","", true);
//        ModeloEquipo modelo= new ModeloEquipo();
        //2.- SET
//        modelo.setNoEquipo(7000);
//        modelo.setMarca("ACER");
//        modelo.setId_Responsable(100);
//        modelo.setFechaRegistro("curdate()");
//        modelo.setFechaUltimoMantenimiento("");
//        modelo.setStatus(true);
//        equipo.save(modelo);
        //UPDATE
//        ModeloEquipo modelo= new ModeloEquipo();  
//        modelo.setNoEquipo(7000);
//        modelo.setMarca("ASUS");
//        equipo.updateteById(modelo);
        //DELETE 
//        ModeloEquipo modelo= new ModeloEquipo();  
//        equipo.deleteById(7000);
        
        
        //////////////////////////REPASO DE INNER JOIN///////////////////////////////////////
        //Ya no quiero ver numeros , quiero ver el nombre del responsable y su equipo asigando
    }

}
