package BD;

import Conexion.Conexion;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author Abraham
 */
//Esta clase se puede considerar como DAO
//Objeto de acceso de Datos
public class EquipoController extends EquipoMetodos {

    /**
     * A esto se le conoce como CRUD
     */
    
    @Override
    public void findByAll() {
        try {
            // se crean las herramientas necesarias para conectar a la base de datos
            Statement sentenciaSQL = null;
            Conexion conecta = new Conexion();
            conecta.Conectar();
            sentenciaSQL = conecta.getSentenciaSQL();
            String sql = "";
            ResultSet cdr;
            sql = "select * from equipo where  status=1";
            cdr = sentenciaSQL.executeQuery(sql);
            while (cdr.next()) {
                System.out.println("Los valores son los  siguientes \n");
                System.out.println("noEquipo: " + cdr.getString("noEquipo") + "\nmarca: '" + cdr.getString("marca") + "'\nid_responsable: " + cdr.getString("id_responsable") + "\nfechaRegistro: '" + cdr.getString("fechaRegistro") + "'\nfechaUltimoMantenimiento: '" + cdr.getString("fechaUltimoMantenimiento") + "'\nstatus: " + cdr.getString("status") + "\n ");
            }
            conecta.cerrar();
        } catch (Exception e) {
            System.out.println("error de Equipo Controller metodo findByiD " + e);
        }
    }

    @Override
    public void findById(int id) {
        try {
            // se crean las herramientas necesarias para conectar a la base de datos
            Statement sentenciaSQL = null;
            Conexion conecta = new Conexion();
            conecta.Conectar();
            sentenciaSQL = conecta.getSentenciaSQL();
            String sql = "";
            ResultSet cdr;
            sql = "select * from equipo where noEquipo=" + id + " and status=1";
            cdr = sentenciaSQL.executeQuery(sql);
            while (cdr.next()) {
                System.out.println("El valor que encontre con este id " + id + " es el siguiente \n");
                System.out.println("noEquipo: " + id + "\nmarca: '" + cdr.getString("marca") + "'\nid_responsable: " + cdr.getString("id_responsable") + "\nfechaRegistro: '" + cdr.getString("fechaRegistro") + "'\nfechaUltimoMantenimiento: '" + cdr.getString("fechaUltimoMantenimiento") + "'\nstatus: " + cdr.getString("status") + " ");
            }
            conecta.cerrar();
        } catch (Exception e) {
            System.out.println("error de Equipo Controller metodo findByiD " + e);
        }
    }

    @Override
    public void findByMarca(String marca) {
        try {
            // se crean las herramientas necesarias para conectar a la base de datos
            Statement sentenciaSQL = null;
            Conexion conecta = new Conexion();
            conecta.Conectar();
            sentenciaSQL = conecta.getSentenciaSQL();
            String sql = "";
            ResultSet cdr;
            sql = "select * from equipo where marca LIKE  '" + marca + "%' and status=1";
            cdr = sentenciaSQL.executeQuery(sql);
            while (cdr.next()) {
                System.out.println("El valor que encontre con esta marca " + marca + " es el siguiente \n");
                System.out.println("noEquipo: " + cdr.getString("noEquipo") + "\nmarca: '" + cdr.getString("marca") + "'\nid_responsable: " + cdr.getString("id_responsable") + "\nfechaRegistro: '" + cdr.getString("fechaRegistro") + "'\nfechaUltimoMantenimiento: '" + cdr.getString("fechaUltimoMantenimiento") + "'\nstatus: " + cdr.getString("status") + "\n ");
            }
            conecta.cerrar();
        } catch (Exception e) {
            System.out.println("error de Equipo Controller metodo findByiD " + e);
        }
    }

    @Override
    public void deleteById(int id) {
        try {
            // se crean las herramientas necesarias para conectar a la base de datos
            Statement sentenciaSQL = null;
            Conexion conecta = new Conexion();
            conecta.Conectar();
            sentenciaSQL = conecta.getSentenciaSQL();
            String sql = "";
            sql = "update equipo set status=" + false + " where noEquipo=" + id + "";
            System.out.println(""+sql);
            sentenciaSQL.executeUpdate(sql);
            System.out.println("Se a eliminado correctamente");
            conecta.cerrar();
        } catch (Exception e) {
            System.out.println("error de Equipo Controller metodo deleteByiD " + e);
        }
    }

    @Override
    public void updateteById(ModeloEquipo modelo) {
        try {
            // se crean las herramientas necesarias para conectar a la base de datos
            Statement sentenciaSQL = null;
            Conexion conecta = new Conexion();
            conecta.Conectar();
            sentenciaSQL = conecta.getSentenciaSQL();
            String sql = "";
            sql = "update equipo set marca='" + modelo.getMarca() + "' where noEquipo=" + modelo.getNoEquipo() + "";
            System.out.println("" + sql);
            sentenciaSQL.executeUpdate(sql);
            System.out.println("Se ha actualizado correctamente");
            conecta.cerrar();
        } catch (Exception e) {
            System.out.println("error de Equipo Controller metodo updateByiD " + e);
        }
    }

    @Override
    public void save(ModeloEquipo modelo) {
        try {
            // se crean las herramientas necesarias para conectar a la base de datos
            Statement sentenciaSQL = null;
            Conexion conecta = new Conexion();
            conecta.Conectar();
            sentenciaSQL = conecta.getSentenciaSQL();
            String sql = "";
            sql = "insert into equipo values(" + modelo.getNoEquipo() + ",'" + modelo.getMarca() + "'," + modelo.getId_Responsable() + "," + modelo.getFechaRegistro() + ",'" + modelo.getFechaUltimoMantenimiento() + "'," + modelo.isStatus() + ");";
            System.out.println("" + sql);
            sentenciaSQL.executeUpdate(sql);
            System.out.println("Se ha insertado correctamente");
            conecta.cerrar();
        } catch (Exception e) {
            System.out.println("error de Equipo Controller metodo save " + e);
        }
    }

}
