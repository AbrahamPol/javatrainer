package BD;

/**
 *
 * @author Abraham
 */
public class ModeloEquipo {
//+--------------------------+------------+------+-----+---------+-------+
//| Field                    | Type       | Null | Key | Default | Extra |
//+--------------------------+------------+------+-----+---------+-------+
//| noEquipo                 | int(11)    | NO   | PRI | NULL    |       |
//| marca                    | text       | YES  |     | NULL    |       |
//| id_responsable           | int(11)    | YES  | MUL | NULL    |       |
//| fechaRegistro            | date       | YES  |     | NULL    |       |
//| fechaUltimoMantenimiento | date       | YES  |     | NULL    |       |
//| status                   | tinyint(1) | YES  |     | NULL    |       |
//+--------------------------+------------+------+-----+---------+-------+

    /**
     * Se declaran las variables a utilizar, conforme en la base de datos
     */
    private int noEquipo;
    private String marca;
    private int id_Responsable;
    private String fechaRegistro;
    private String fechaUltimoMantenimiento;
    private boolean status;

    public int getNoEquipo() {
        return noEquipo;
    }

    public void setNoEquipo(int noEquipo) {
        this.noEquipo = noEquipo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public int getId_Responsable() {
        return id_Responsable;
    }

    public void setId_Responsable(int id_Responsable) {
        this.id_Responsable = id_Responsable;
    }

    public String getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public String getFechaUltimoMantenimiento() {
        return fechaUltimoMantenimiento;
    }

    public void setFechaUltimoMantenimiento(String fechaUltimoMantenimiento) {
        this.fechaUltimoMantenimiento = fechaUltimoMantenimiento;
    }

    public boolean isStatus() {
        return status;
    }

    public boolean getStatus(){
        return status;
    }
    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "ModeloEquipo{" + "noEquipo=" + noEquipo + ", marca=" + marca + ", id_Responsable=" + id_Responsable + ", fechaRegistro=" + fechaRegistro + ", fechaUltimoMantenimiento=" + fechaUltimoMantenimiento + ", status=" + status + '}';
    }

    public ModeloEquipo(int noEquipo,String marca, int id_Responsable, String fechaRegistro, String fechaUltimoMantenimiento, boolean status) {
        this.noEquipo = noEquipo;
        this.marca = marca;
        this.id_Responsable = id_Responsable;
        this.fechaRegistro = fechaRegistro;
        this.fechaUltimoMantenimiento = fechaUltimoMantenimiento;
        this.status = status;
    }

    public ModeloEquipo() {
    }

    
    
}
