package ARREGLOS;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Abraham
 */
public class Listas {

    public static void main(String args[]) {
        //LISTAS
        /**
         * ALT+SHIFT+F (FORMATO AL CODIGO)
         *
         * El uso de listas en Java es una forma útil de almacenar y manipular
         * grandes volúmenes de datos, tal como haríamos en una matriz o
         * arreglo, pero con una serie de ventajas que hacen de este tipo de
         * variables las preferidas para el procesamiento de grandes cantidades
         * de información.
         */

//        Map<String, Object> model;
//        model.put("1", "Hola");
        List<String> ejemploLista = new ArrayList<String>();
        ejemploLista.add("Juan");
        ejemploLista.add("Pedro");
        ejemploLista.add("José");
        ejemploLista.add("María");
        ejemploLista.add("Sofía");

       // System.out.println("" + ejemploLista.get(0));
        //ejemploLista.remove(0);
        //ejemploLista.remove("Juan");
        //System.out.println(ejemploLista);
//        for (int i = 0; i <= ejemploLista.size() - 1; i++) {
//            System.out.println(ejemploLista.get(i));
//        }
//        ejemploLista.clear();//limpiar
//        ejemploLista.isEmpty();//saber si esta vacia
//        ejemploLista.contains("José");//si contiene jose
//        ejemploLista.set(1, "Félix");//sustituir en esa posicion el nuevo valor
//        ejemploLista.subList(0, 2)//encontrar los valores dentro de ese rango
//        for (String nombre : ejemploLista) {
//
//            System.out.println(nombre);
//        }

    }
}
