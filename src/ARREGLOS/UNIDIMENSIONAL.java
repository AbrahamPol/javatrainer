
package ARREGLOS;

/**
 *
 * @author Abraham
 */
public class UNIDIMENSIONAL {
    
       public static void main(String args[]) {
        //ARREGLOS UNIDMENSIONALES
        /**
         * ¿Que es un arreglo? Los arreglos se pueden definir como objetos en
         * los que podemos guardar mas de una variable, es decir, al tener un
         * unico arreglo, este puede guardar multiples variables de acuerdo a su
         * tamaño o capacidad, es importante recordar que las variables
         * guardadas deben ser del mismo tipo, por ejemplo: Si tenemos un
         * arreglo de tipo Numerico que puede almacenar 10 variables, solo podra
         * almacenar 10 numeros diferentes, no otras variables como caracteres o
         * Strings.
         */
        //Instanciar un arreglo tipo unidimensional
        /**
         * Empiezan desde 0 hasta n..............
         * tipo_dedato [] nombre_variable; //tambien puede declararse
         * “tipo_dedato nombre_variable[];”
         * String [] sandy; || int sandy[];
         */

         
        /**
         * String array[]; //donde array es un arreglo que guardara variables
         * tipo Strings double array2[]; //donde array2 es un arreglo que
         * guardara variabels tipo double int [] array3; //donde array3 es un
         * arreglo que guardara variables tipo int(numericas)
         */
        /**
         * int array1[]; //declaracion arreglo de tipo numerico array1 = new
         * int[3]; //tamaño del arreglo es 3 elementos //asignacion de valores a elementos del arreglo; 
        
         * array[0] =2; 
         *
         * array[1] = 10; 
         *
         * arra[2] = 7; 
         *
         * System.out.println("El valor de la posicion 1 en el arreglo es" +
         * array[0]);
         */
           
           
//           String sandy[] = new String[2];
//           sandy[0] = "Sandra";
//           sandy[1] = "Sabas";
//           for (int i = 0; i < sandy.length; i++) {
//               System.out.println("Valor de sandy[" + i + "] es : " + sandy[i]);
//           }
           
    }
}
