package POO;

/**
 *
 * @author Abraham
 */
public class ClasePropiedades {
    /*
     * ¿Que es la POO?
     La POO es un paradigma de programación (un modelo), 
     la cual permite relacionar los objetos del mundo real y de alguna manera 
     Llevar estos conceptos a la programación.
     */

    /**
     * ¿Que es una clase en POO? Las clases en Java son básicamente una
     * plantilla que sirve para crear un objeto. Si imaginásemos las clases en
     * el mundo en el que vivimos, podríamos decir que la clase “persona” es una
     * plantilla sobre cómo debe ser un ser humano.
     *
     * ¿Que contiene una clase en POO? Una clase en POO, contiene
     * metodos,variables,constructores,metodos de acceso, Estos con su propio
     * modificador de acceso:Public,Protected,Private
     * https://www.aprenderaprogramar.com/index.php?option=com_content&view=article&id=
     * 665:public-private-y-protected-javatipos-de-modificadores-de-acceso-visibilidad-en-clases-
     * subclases-cu00693b&catid=68&Itemid=188
     *
     * ¿Que es un metodo en POO? Un método en Java es un conjunto de
     * instrucciones definidas dentro de una clase, que realizan una determinada
     * tarea y a las que podemos invocar mediante un nombre.
     *
     *
     * ¿Que es un constructor en POO? Un constructor es un método especial de
     * una clase que se llama automáticamente siempre que se declara un objeto
     * de esa clase. Su función es inicializar el objeto y sirve para
     * asegurarnos que los objetos siempre contengan valores válidos.
     *
     * ¿Que es una variable global? Las variables de ámbito global, o de
     * instancia, son aquellas que pertenecen a cada instancia concreta de la
     * clase donde han sido declaradas.
     *
     * ¿Que es un objeto en POO? Es una entidad existente en la memoria del ordenador
     * que tiene unas propiedades (atributos o datos sobre sí mismo almacenados
     * por el objeto) y unas operaciones disponibles específicas (métodos).
     */
}
