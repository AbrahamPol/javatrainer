package POO;

/**
 *
 * @author Abraham
 */
public class PilaresPOO {
    /**
     * ¿Que son los pilares de la POO? Son los fundamentos por los cuales se
     * rige la programacion orientado a objetos, el uso de estos pilares, son
     * eficaces a la hora de desarrollar.
     *
     * ¿Que es herencia en POO? El concepto fundamental de la herencia es el
     * proceso en el que un objeto adquiere características de otro objeto.
     *
     * ¿Que es polimorfismo en POO? El polimorfismo es una palabra de origen
     * griego que significa “muchas formas”, es un término un poco complejo de
     * entender, que a veces sólo se lo entiende con la práctica. El concepto
     * dice que es la capacidad de un objeto para comportarse de diferentes
     * formas de acuerdo al mensaje enviado
     *
     *
     * ¿Que es abstraccion en POO? El término abstracción consiste en ver a algo
     * como un todo sin saber cómo está formado internamente. Que hacer, mas no
     * como hacerlo
     *
     * ¿Que es encapsulamiento en POO? Este concepto consiste en la ocultación
     * del estado o de los datos miembro de un objeto, de forma que sólo es
     * posible modificar los mismos mediante los métodos definidos para dicho
     * objeto.
     */
}
