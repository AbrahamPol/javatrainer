
package POO;

/**
 *
 * @author Abraham
 */
public class Circulo extends Figura{

    private double radio;
    private double area;

    public Circulo(double radio) {
        this.setRadio(radio);
    }
  
    
    
    public double getRadio() {
        return radio;
    }

    public void setRadio(double radio) {
        this.radio = radio;
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }  
    
    @Override
    public void Caracteristicas() {
        System.out.println("Este es un circulo");
    }

    @Override
    public void area() {
       area=3.1416*(radio*radio);
        System.out.println("El area del circulo es: "+area);
    }
    
}
