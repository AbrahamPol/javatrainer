package POO;

/**
 *
 * @author Abraham
 */
public class Triangulo extends Figura {

    private double area;
    private double base;
    private double altura;

    public Triangulo(double base, double altura) {
        this.setBase(base);
        this.setAltura(altura);
    }

    
    
    
    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

    @Override
    public void Caracteristicas() {
        System.out.println("Esto es un triangulo");
    }

    @Override
    public void area() {
        area = (base * altura) / 2;
        System.out.println("El area del triangulo es: "+area);
    }

}
